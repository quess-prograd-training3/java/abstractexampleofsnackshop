package com.example;

public class SnackShopImplementation extends SnackShop{
    private int teaPrice;
    private int coffeePrice;
    private int puffPrice;
    private int samosaPrice;
    private int bunPrice;


    public SnackShopImplementation(int teaPrice, int coffeePrice, int puffPrice, int samosaPrice, int bunPrice) {
        this.teaPrice = teaPrice;
        this.coffeePrice = coffeePrice;
        this.puffPrice = puffPrice;
        this.samosaPrice = samosaPrice;
        this.bunPrice = bunPrice;
    }

    @Override
    public int printBill(){
        int total=teaPrice+coffeePrice+samosaPrice+puffPrice+bunPrice;
        total+=(int)((teaPrice*0.05)+(coffeePrice*0.03)+(puffPrice*0.1)*(samosaPrice*0.2)+(bunPrice*0.25));
        return total;
    }

    public int getTeaPrice() {
        return teaPrice;
    }

    public int getCoffeePrice() {
        return coffeePrice;
    }

    public int getPuffPrice() {
        return puffPrice;
    }

    public int getSamosaPrice() {
        return samosaPrice;
    }

    public int getBunPrice() {
        return bunPrice;
    }
}
